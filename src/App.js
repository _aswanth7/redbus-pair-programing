import "./App.css";
import SignUp from "./components/SignUp";
import SignIn from "./components/SignIn";
import { BrowserRouter, Switch, Route, Router } from "react-router-dom";
import Dashboard from "./components/Dashboard";
import { createBrowserHistory as history } from "history";

function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <Router history={history()}>
          <Switch>
            <Route path="/" exact component={SignIn}></Route>
            <Route path="/dashboard" exact component={Dashboard}></Route>

            <Route path="/signup" exact component={SignUp}></Route>
          </Switch>
        </Router>
      </div>
      ;
    </BrowserRouter>
  );
}

export default App;
