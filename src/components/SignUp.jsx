import React, { Component } from "react";
import "./Css/style.css";
import axios from "axios";
import { Redirect } from "react-router-dom";

export class SignUp extends Component {
  state = {
    name: "",
    nameError: "",
    email: "",
    emailError: "",
    password: "",
    passwordError: "",
    error: "",
    redirect: false,
  };
  checkName = (e) => {
    this.setState({ name: e.target.value });
    if (e.target.value === "") {
      this.setState({ nameError: "Name should not empty" });
    } else {
      this.setState({ nameError: "" });
    }
  };

  checkEmail = (e) => {
    this.setState({ email: e.target.value });
    const re = /.+@.+.[A-Za-z]+$/;
    if (e.target.value === "") {
      this.setState({ emailError: "Email should not empty" });
    } else if (!re.test(e.target.value)) {
      this.setState({ emailError: "Invalid Email" });
    } else {
      this.setState({ emailError: "" });
    }
  };

  checkPassword = (e) => {
    this.setState({ password: e.target.value });
    if (e.target.value === "") {
      this.setState({ passwordError: "Password should not empty" });
    } else if (e.target.value.length < 6) {
      this.setState({
        passwordError: "Password should have minimum 6 characters",
      });
    } else {
      this.setState({ passwordError: "" });
    }
  };
  checkForm = (e) => {
    if (
      (e.target.name.value =
        "" || e.target.email.value === "" || e.target.password.value === "")
    ) {
      this.setState({ error: "Input field can not be empty" });
      e.preventDefault();
    } else {
      this.setState({ error: "" });
      axios
        .get("http://0.0.0.0:8000/data.json")
        .then((response) => {
          let isUser = response.data.filter(
            (userdata) => userdata.email === e.target.email.value
          );
          if (isUser.length > 0) {
            this.setState({ error: "User already exits" });
          } else {
            console.log("Sign up complete");
            this.setState({ redirect: true });
          }
        })
        .catch((err) => {
          console.error(err);
        });
      e.preventDefault();
    }
  };

  render() {
    return this.state.redirect ? (
      <Redirect to="dashboard" />
    ) : (
      <div className="formContainer">
        <h3>Sign Up</h3>
        <p className="errorShow">{this.state.error}</p>
        <div className="form">
          <form onSubmit={this.checkForm}>
            <div class="form-group">
              <label for="exampleInputEmail1"> Name</label>
              <input
                type="text"
                className="form-control"
                id="username"
                placeholder="Enter Name"
                name="name"
                onChange={this.checkName}
                value={this.state.name}
              />
              <span className="errorShow">{this.state.nameError}</span>
            </div>
            <div className="form-group">
              <label for="exampleInputEmail1">Email address</label>
              <input
                type="email"
                className="form-control"
                id="exampleInputEmail1"
                name="email"
                placeholder="Enter email"
                value={this.state.email}
                onChange={this.checkEmail}
              />
              <span className="errorShow">{this.state.emailError}</span>
            </div>
            <div className="form-group">
              <label for="exampleInputPassword1">Password</label>
              <input
                type="password"
                className="form-control"
                id="exampleInputPassword1"
                placeholder="Password"
                name="password"
                value={this.state.password}
                onChange={this.checkPassword}
              />
              <span className="errorShow">{this.state.passwordError}</span>
            </div>
            <button type="submit" className="btn btn-primary">
              Submit
            </button>
          </form>
        </div>
      </div>
    );
  }
}

export default SignUp;
