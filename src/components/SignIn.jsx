import React, { Component } from "react";
import "./Css/style.css";
import axios from "axios";
import { Redirect } from "react-router-dom";

export class SignIn extends Component {
  state = {
    email: "",
    emailError: "",
    password: "",
    passwordError: "",
    error: "",
    redirect: false,
  };

  checkEmail = (e) => {
    this.setState({ email: e.target.value });
    const re = /.+@.+.[A-Za-z]+$/;
    if (e.target.value === "") {
      this.setState({ emailError: "Email should not empty" });
    } else if (!re.test(e.target.value)) {
      this.setState({ emailError: "Invalid Email" });
    } else {
      this.setState({ emailError: "" });
    }
  };

  checkPassword = (e) => {
    this.setState({ password: e.target.value });
    if (e.target.value === "") {
      this.setState({ passwordError: "Password should not empty" });
    } else if (e.target.value.length < 6) {
      this.setState({
        passwordError: "Password should have minimum 6 characters",
      });
    } else {
      this.setState({ passwordError: "" });
    }
  };
  checkForm = (e) => {
    try {
      if (e.target.email.value === "" || e.target.password.value === "") {
        this.setState({ error: "Input field can not be empty" });
        e.preventDefault();
      } else {
        this.setState({ error: "" });
        axios
          .get("http://0.0.0.0:8000/data.json")
          .then((response) => {
            let isUser = response.data.filter(
              (userdata) =>
                userdata.password === e.target.password.value &&
                userdata.email === e.target.email.value
            );
            if (isUser.length > 0) {
              console.log("User Logged IN");
              this.setState({
                redirect: true,
              });
            }
          })
          .catch((err) => {
            console.error(err);
          });
        e.preventDefault();
      }
    } catch (err) {
      console.log(err);
    }
  };

  render() {
    return this.state.redirect ? (
      <Redirect to="dashboard" />
    ) : (
      <div className="formContainer">
        <h3>Sign In</h3>
        <p className="errorShow">{this.state.error}</p>
        <div className="form">
          <form onSubmit={this.checkForm}>
            <div className="form-group">
              <label for="exampleInputEmail1">Email address</label>
              <input
                type="email"
                className="form-control"
                id="exampleInputEmail1"
                name="email"
                placeholder="Enter email"
                value={this.state.email}
                onChange={this.checkEmail}
              />
              <span className="errorShow">{this.state.emailError}</span>
            </div>
            <div className="form-group">
              <label for="exampleInputPassword1">Password</label>
              <input
                type="password"
                className="form-control"
                id="exampleInputPassword1"
                placeholder="Password"
                name="password"
                value={this.state.password}
                onChange={this.checkPassword}
              />
              <span className="errorShow">{this.state.passwordError}</span>
            </div>
            <button type="submit" className="btn btn-primary">
              Submit
            </button>
          </form>
        </div>
      </div>
    );
  }
}

export default SignIn;
