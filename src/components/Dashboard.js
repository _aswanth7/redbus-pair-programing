import React, { Component } from "react";

import axios from "axios";

class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      busData: [],
      isLoading: true,
      hasError: false,
    };
  }
  componentDidMount() {
    axios
      .get("http://localhost:8000/data1.json")
      .then((response) => {
        this.setState({
          busData: response.data,
          isLoading: false,
        });
      })
      .catch((err) => {
        console.error(err);
        this.setState({
          hasError: true,
          isLoading: false,
        });
      });
  }

  onsubmit = (event) => {
    event.preventDefault();

    console.log("Clicked Book");
  };
  loading = (<div class="lds-dual-ring"></div>);
  error = (<h2> Error </h2>);

  render() {
    return (
      <div className="container">
        <h1>Bus from Bangalore to Kochi</h1>

        {this.state.isLoading
          ? this.loading
          : this.state.hasError
          ? this.error
          : this.state.busData.map((data) => {
              console.log(data.bus_id);

              return (
                <div key={data.bus_id}>
                  <form onSubmit={this.onsubmit} className="bus">
                    <p>{data.bus_name}</p>
                    <div>
                      <span>Departure</span>
                      <p>{data.departure}</p>
                    </div>
                    <div>
                      <span>Arrival</span>
                      <p>{data.arrival}</p>
                    </div>

                    <p>{`INR ${data.price}`}</p>
                    <p>{`Seat available ${data.seat_available}`}</p>
                    <input type="submit" value="Book"></input>
                  </form>
                </div>
              );
            })}
      </div>
    );
  }
}

export default Dashboard;
